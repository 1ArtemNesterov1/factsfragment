package com.example.dzfragment;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<FactAnimal> convertRequest(List<RequestModel> requestModel) {

        List<FactAnimal> facts = new ArrayList<>();

        for (int i = 0; i < requestModel.size(); i++) {

            facts.add(new FactAnimal(
                    requestModel.get(i).getId(),
                    requestModel.get(i).getCreatedAt(),
                    requestModel.get(i).getText(),
                    requestModel.get(i).getType()));
        }
        return facts;
    }
}
