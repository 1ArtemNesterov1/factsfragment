package com.example.dzfragment;

import java.util.List;

public class RequestModel {


    private String id;

    private String type;

    private String createdAt;

    private String user;

    private String text;


    public RequestModel(String id, String type, String createdAt, String user, String text) {
        this.id = id;
        this.type = type;
        this.createdAt = createdAt;
        this.user = user;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
