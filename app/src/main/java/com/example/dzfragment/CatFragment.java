package com.example.dzfragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CatFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Disposable disposable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cat, container, false);
        ButterKnife.bind(this, view);

        getDataFromServer();

        return view;
    }

    private void getDataFromServer() {
        disposable = ApiService.getFactsByAnimalAndAmount("cat", "100")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<RequestModel>>() {
                    @Override
                    public void accept(List<RequestModel> requestModels) throws Exception {
                        List<FactAnimal> factAnimalList = Converter.convertRequest(requestModels);

                        AdapterFacts adapterFacts = new AdapterFacts(factAnimalList);
                        recyclerView.setAdapter(adapterFacts);
                    }
                });
    }

}
