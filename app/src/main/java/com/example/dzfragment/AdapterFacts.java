package com.example.dzfragment;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.List;

class AdapterFacts extends RecyclerView.Adapter<AdapterFacts.Holder> {

    Context context;
    List<FactAnimal>factAnimals;

    public AdapterFacts(Context context, List<FactAnimal> factAnimals) {
        this.context = context;
        this.factAnimals = factAnimals;
    }

    public AdapterFacts(List<FactAnimal> factAnimalList) {
     this.factAnimals = factAnimalList;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.fragment_cat, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Glide.with(context).load(factAnimals.get(position).getType()).into((ImageView) holder.itemView);
    }


    @Override
    public int getItemCount() {
        return factAnimals.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        TextView textView2;
        TextView textView3;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView.findViewById(R.id.imageView2);
            textView.findViewById(R.id.namePet);
            textView.findViewById(R.id.factPet);
            textView.findViewById(R.id.date);
        }
    }
}
